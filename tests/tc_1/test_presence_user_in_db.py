import allure
import pytest


@pytest.mark.usefixtures("postgres_query")
class TestPresenceNewUser:

    @allure.description("This test check presence of user in data base")
    def test_presence_user_in_db(self, postgres_query):

        username = "new_user@gmail.com"

        assert postgres_query.is_user_in_db(username), f"No user with name {username} in data base"
