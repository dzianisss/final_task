import allure
import pytest


@pytest.mark.usefixtures("login_as_admin")
class TestCreateNewUser:

    @allure.description("This test create new user and check result of this action")
    def test_create_new_user(self, admin_page):

        new_user = admin_page.create_new_user("new_user@gmail.com", "qw12erty")

        assert new_user, "Creating user was failed. User existed or entered wrong data."
