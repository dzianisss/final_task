import allure
import pytest


@pytest.mark.usefixtures("navigate_to_login_page")
class TestLoginNewUser:

    @allure.description("This test checks how new user logins")
    def test_login_new_user(self, login_page):

        login_page.login("new_user@gmail.com", "qw12erty")

        assert login_page.is_login_success(), "Login was failed. User does not exist or wrong credentials"
