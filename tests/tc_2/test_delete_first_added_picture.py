import allure
import pytest


@pytest.mark.usefixtures("login_as_admin", "navigate_to_home_page")
class TestDeleteAddedPicture:

    @allure.description("This test check adding new pet at store")
    def test_delete_added_image(self, admin_page, home_page):

        image_number = "1"

        count_before_delete = home_page.get_elements_list()
        result_of_delete = admin_page.delete_image(image_number)
        count_after_delete = home_page.get_elements_list()

        assert count_before_delete != count_after_delete and (result_of_delete is True), \
            "Image does not exist or delete are failed"
