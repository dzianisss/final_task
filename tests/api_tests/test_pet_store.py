import allure


class TestPetStore:
    @allure.description("This test check adding new pet at store")
    def test_adding_pet(self, api_service):

        response = api_service.add_new_pet(27, "John")

        assert response.status_code == 200, f"Not expected statuscode. Status code {response.status_code}"

    @allure.description("This test check presence of new pet at store")
    def test_presence_pet(self, api_service):

        response = api_service.get_pet_by_id(27)

        assert response["id"] == 27, "Pet with id does not exist"

    @allure.description("This test update name of pet and check status code")
    def test_update_pet_name(self, api_service):

        response = api_service.update_pets_name(27, "Deb")

        assert response.status_code == 200, f"Not expected statuscode. Status code {response.status_code}"

    @allure.description("This test check existing new pets name in his info")
    def test_presence_update_name(self, api_service):

        response = api_service.get_pet_by_id(27)

        assert response["name"] == "Deb", "Failed.Name does not update"
