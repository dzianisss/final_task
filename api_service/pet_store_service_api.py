import requests


class PetsApiService:

    BASE_URL = "https://petstore.swagger.io/v2"

    HEADERS = {
        'accept': 'application/json',

        'Content-Type': 'application/json'
              }

    @staticmethod
    def _create_request_body(pet_id, pet_name):

        return {
                "id": pet_id,
                "category": {
                                    "id": 0,
                                    "name": "string"
                                },
                "name": pet_name,
                "photoUrls": [
                              "string"
                             ],
                "tags": [
                            {
                                "id": 0,
                                "name": "string"
                            }
                        ],
                "status": "available"
                }

    def _create_endpoint(self, endpoint):
        return self.BASE_URL + endpoint

    def _get(self, endpoint):
        url = self._create_endpoint(endpoint)
        response = requests.get(url)
        return response

    def _post(self, endpoint, request_body):
        url = self._create_endpoint(endpoint)
        response = requests.post(url=url, json=request_body, headers=self.HEADERS)
        return response

    def _put(self, endpoint, request_body):
        url = self._create_endpoint(endpoint)
        response = requests.put(url=url, json=request_body, headers=self.HEADERS)
        return response

    def add_new_pet(self, pet_id, pet_name):
        endpoint = "/pet"
        request_body = self._create_request_body(pet_id, pet_name)
        response = self._post(endpoint, request_body)
        return response

    def update_pets_name(self, pet_id, new_pet_name):
        endpoint = "/pet"
        request_body = self._create_request_body(pet_id, new_pet_name)
        response = self._put(endpoint, request_body)
        return response

    def get_pet_by_id(self, pet_id):
        endpoint = f"/pet/{pet_id}"
        response = self._get(endpoint)
        return response.json()
