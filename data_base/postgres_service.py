class PostgresService:
    def __init__(self, connection):
        self.connection = connection
        self.cursor = self.connection.cursor()

    def execute_query(self, query):
        self.cursor.execute(query)
        response = self.cursor.fetchall()
        return response
