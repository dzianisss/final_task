import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

import psycopg2
from data_base.postgress_query import PostgresQuery

from pages.login_page import LoginPage
from pages.home_page import HomePage
from pages.admin_page import AdminPage

from api_service.pet_store_service_api import PetsApiService


@pytest.fixture()
def navigate_to_admin_page(admin_page, chromedriver):
    admin_page.navigate()
    yield
    chromedriver.quit()


@pytest.fixture()
def navigate_to_login_page(login_page, chromedriver):
    login_page.navigate()
    yield
    chromedriver.quit()


@pytest.fixture()
def navigate_to_home_page(home_page, chromedriver):
    home_page.navigate()
    yield
    chromedriver.quit()


@pytest.fixture()
def login_as_admin(login_page, chromedriver):
    login_page.navigate()
    login_page.login("admin", "password")
    yield
    chromedriver.quit()


@pytest.fixture()
def open_connection_with_db():
    with psycopg2.connect(dbname='postgres', user='postgres',
                          password='postgres', host='localhost') as connection:
        return connection


@pytest.fixture()
def postgres_query(open_connection_with_db):
    return PostgresQuery(open_connection_with_db)


@pytest.fixture()
def chromedriver():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--headless')
    return webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)


@pytest.fixture()
def login_page(chromedriver):
    return LoginPage(chromedriver)


@pytest.fixture()
def home_page(chromedriver):
    return HomePage(chromedriver)


@pytest.fixture()
def admin_page(chromedriver):
    return AdminPage(chromedriver)


@pytest.fixture()
def api_service():
    return PetsApiService()
