from pages.base_page import BasePage


class LoginPage(BasePage):

    URL = "http://localhost:8000/admin/login/"
    USER_NAME_INPUT_LOCATOR = "//input[@name='username']"
    PASSWORD_INPUT_LOCATOR = "//input[@name='password']"
    LOGIN_BUTTON_LOCATOR = "//input[@type='submit']"
    USER_TOOLS_LOCATOR = "//div[@id='user-tools']"

    def enter_user_name(self, user_name):
        self.enter_text(self.USER_NAME_INPUT_LOCATOR, user_name)

    def enter_password(self, password):
        self.enter_text(self.PASSWORD_INPUT_LOCATOR, password)

    def click_login_button(self):
        self.click(self.LOGIN_BUTTON_LOCATOR)

    def login(self, user_name, password):
        self.enter_user_name(user_name)
        self.enter_password(password)
        self.click_login_button()

    def is_login_success(self):
        return self.is_element_present(self.USER_TOOLS_LOCATOR)

    def navigate(self):
        self.open_url(self.URL)


