from pages.base_page import BasePage


class AdminPage(BasePage):

    URL = "http://localhost:8000/admin/"

    ADD_NEW_USER_LOCATOR = "//a[@href='/admin/auth/user/add/']"
    INPUT_USER_NAME_LOCATOR = "//input[@name='username']"
    INPUT_PASSWORD_LOCATOR = "//input[@name='password1']"
    CONFIRM_PASSWORD_LOCATOR = "//input[@name='password2']"
    SAVE_BUTTON_LOCATOR = "//input[@value='Save']"
    ERROR_TEXT_LOCATOR = "//p[@class='errornote']"
    POST_LIST_BUTTON_LOCATOR = "//a[@href='/admin/app/post/']"
    SELECT_IMAGES_LOCATOR = "//a[@href='/admin/app/post/{}/change/']"
    STAFF_PERMISSION_LOCATOR = "//input[@id='id_is_staff']"
    SELECT_AND_SAVE_BUTTON_LOCATOR = "//input[@name='_continue']"
    DELETE_IMAGES_BUTTON_LOCATOR = "//a[@class='deletelink']"
    SUBMIT_DELETE_LOCATOR = "//input[@type='submit']"
    LINK_IMAGES_LOCATOR = "//textarea[@class='vLargeTextField']"
    VIEW_SITE_LOCATOR = "//a[@href='/']"

    def navigate(self):
        self.open_url(self.URL)

    def enter_username(self, user_name):
        self.enter_text(self.INPUT_USER_NAME_LOCATOR, user_name)

    def enter_password(self, password):
        self.enter_text(self.INPUT_PASSWORD_LOCATOR, password)

    def confirm_password(self, password):
        self.enter_text(self.CONFIRM_PASSWORD_LOCATOR, password)

    def create_new_user(self, username, password):
        self.click(self.ADD_NEW_USER_LOCATOR)
        self.enter_username(username)
        self.enter_password(password)
        self.confirm_password(password)
        self.click(self.SELECT_AND_SAVE_BUTTON_LOCATOR)
        if self.is_element_present(self.ERROR_TEXT_LOCATOR):
            return False
        else:
            self.click(self.STAFF_PERMISSION_LOCATOR)
            self.click(self.SAVE_BUTTON_LOCATOR)
            return True

    def get_post_link(self, image_number):
        if self.is_image_exist(image_number):
            self.click(self.SELECT_IMAGES_LOCATOR.format(image_number))
            return self.get_text(self.LINK_IMAGES_LOCATOR)

    def delete_image(self, image_number):
        if self.is_image_exist(image_number):
            self.click(self.SELECT_IMAGES_LOCATOR.format(image_number))
            self.click(self.DELETE_IMAGES_BUTTON_LOCATOR)
            self.click(self.SUBMIT_DELETE_LOCATOR)
            self.click(self.VIEW_SITE_LOCATOR)
            return True
        else:
            self.click(self.VIEW_SITE_LOCATOR)
            return False

    def is_image_exist(self, image_number):
        self.navigate()
        self.click(self.POST_LIST_BUTTON_LOCATOR)
        return self.is_element_present(self.SELECT_IMAGES_LOCATOR.format(image_number))


