from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver

    @staticmethod
    def _get_locator_by_type(locator):
        if "//" in locator:
            return By.XPATH

        return By.CSS_SELECTOR

    def _element(self, locator, time_out_sec=5):
        locator_by_type = self._get_locator_by_type(locator)

        element = WebDriverWait(self.driver, time_out_sec).until(
            EC.presence_of_element_located((locator_by_type, locator)))
        return element

    def elements_list(self, locator, time_out_sec=5):
        locator_by_type = self._get_locator_by_type(locator)

        elements = WebDriverWait(self.driver, time_out_sec).until(
            EC.presence_of_all_elements_located((locator_by_type, locator)))
        return elements

    def enter_text(self, locator, text):
        element = self._element(locator)
        element.send_keys(text)

    def click(self, locator):
        element = self._element(locator)
        element.click()

    def get_text(self, locator):
        element = self._element(locator)
        return element.text

    def is_element_present(self, locator):
        try:
            self._element(locator)
            return True
        except TimeoutException:
            return False

    def open_url(self, url):
        self.driver.get(url)

    def get_attribute_text(self, locator):
        element = self._element(locator)

        return element.get_attribute("src")
