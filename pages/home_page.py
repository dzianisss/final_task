from pages.base_page import BasePage


class HomePage(BasePage):
    URL = "http://localhost:8000/"
    ADMIN_BUTTON_LOCATOR = "//a[@href='/admin']"
    IMAGES_LOCATOR = "//img[@class='card-img-top']"

    def click_to_admin_button(self):
        self.click(self.ADMIN_BUTTON_LOCATOR)

    def navigate(self):
        self.open_url(self.URL)

    def get_elements_list(self):

        return self.elements_list(self.IMAGES_LOCATOR)



